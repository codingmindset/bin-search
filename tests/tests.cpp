#include "catch.hpp"
#include "../src/search.hpp"
#include <vector>
#include <string>


using namespace std;



TEST_CASE("TEST TO PASS", "[test1]")
{
  vector<int> D1 = {1, 2, 3, 4, 5};
  vector<char> D2 = {'a', 'e', 'i', 'o', 'u'};


  CHECK( binarySearch(D1 , 4) == 3);
  CHECK( binarySearch(D2, 'i') == 2);
  CHECK( binarySearch(D1 , 7) == -1);
}

